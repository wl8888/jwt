package com.example.jwt;


import com.example.jwt.dto.Payload;
import com.example.jwt.dto.UserDetail;
import com.example.jwt.utils.JwtUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JwtApplicationTests {

    @Test
    public  void contextLoads() {
    }

    @Autowired
    private JwtUtils jwtUtils;

    @Test
    public void test() throws InterruptedException {
        // 生成jwt
        String jwt = jwtUtils.createJwt(UserDetail.of(112L, "lele"));
        System.out.println("jwt = " + jwt);


        // 解析jwt
        Payload payload = jwtUtils.parseJwt(jwt);
        System.out.println("payload = " + payload);
    }

}
