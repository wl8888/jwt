package com.example.jwt.dto;

import lombok.Data;

//载荷对象
@Data
public class Payload {
    private String jti;//tocken的id
    private UserDetail userDetail;//用户数据
}
